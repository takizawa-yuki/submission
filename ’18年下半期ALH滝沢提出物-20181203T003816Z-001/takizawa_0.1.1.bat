@echo off
echo;
echo 〜注意事項〜
echo フォルダ名には環境依存文字でなければ入力できます（たぶん）
echo フォルダ数、サブフォルダ数には、必ず半角整数で入力してください。
echo 数字以外の入力の場合、フォルダが作成されません。ご注意ください。
echo このバージョンは0.1.1です。単純なロジックなので過度な期待はしないでください。
echo;

rem 初期フォルダ確認
if not exist 課題フォルダ (
md 課題フォルダ
cd 課題フォルダ
) else (
echo すでに「課題フォルダ」が存在します。
echo;
pause
exit /b 0
)

rem サブフォルダ作成
set /p filename="フォルダ名を入力してください："
set /p number="フォルダ数を入力してください："
rem その次のフォルダも作成
set /p subfilename="サブフォルダ名を入力してください："
set /p subfilenumber="サブフォルダ数を入力してください："

setlocal enabledelayedexpansion
for /l %%n in (1,1,%number%) do ( 
 set num=0%%n
 set num=!num:~-2,2!
 mkdir !num!_%filename%
 
 cd !num!_%filename%
for /l %%i in (1,1,%subfilenumber%) do (
set num2=0%%i
set num2=!num2:~-2,2!
mkdir !num2!_%subfilename%
)

cd ..
)
endlocal

cd ..
echo;

echo 作成が完了しました。

pause